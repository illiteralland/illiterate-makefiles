# Copyright (C) 2023 Krzysztof Stachowiak
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>

# Default target
# --------------

default:

# Convenience variables
# ---------------------

__SAN-FLAGS = -fsanitize=address -fsanitize=leak -fsanitize=undefined -static-libasan
__COV-FLAGS = -fprofile-arcs -ftest-coverage

# Toolchain flags
# ---------------

__COMMON-CFLAGS = -Wall -Wextra -std=c11
__COMMON-LDFLAGS = -lm

__RELEASE-CFLAGS = $(__COMMON-CFLAGS) -O2 $(_EXTRA-RELEASE-CFLAGS)
__RELEASE-LDFLAGS = $(__COMMON-LDFLAGS) $(_EXTRA-RELEASE-LDFLAGS)

__DEBUG-CFLAGS = $(__COMMON-CFLAGS) -O0 -g3 $(_EXTRA-RELEASE-CFLAGS)
__DEBUG-LDFLAGS = $(__COMMON-LDFLAGS) $(_EXTRA-DEBUG-LDFLAGS)

# Object definitions
# ------------------

__RELEASE-OBJECTS = $(_LIB-SOURCES:%.c=%.o)
__DEBUG-OBJECTS = $(_LIB-SOURCES:%.c=%d.o)
__SAN-OBJECTS = $(_LIB-SOURCES:%.c=%sd.o)
__COV-OBJECTS = $(_LIB-SOURCES:%.c=%cd.o)

# Compile targets
# ---------------

%.o: %.c
	$(CC) $(__RELEASE-CFLAGS) -c $< -o $@

%d.o: %.c
	$(CC) $(__DEBUG-CFLAGS) -MD -MP -c $< -o $@

%sd.o: %.c
	$(CC) $(__DEBUG-CFLAGS) $(__SAN-FLAGS) -c $< -o $@

%cd.o: %.c
	$(CC) $(__DEBUG-CFLAGS) $(__COV-FLAGS) -c $< -o $@

# Linker targets
# --------------

lib$(_LIB-NAME).a: $(__RELEASE-OBJECTS)
	$(AR) rcs $@ $^

lib$(_LIB-NAME)d.a: $(__DEBUG-OBJECTS)
	$(AR) rcs $@ $^

lib$(_LIB-NAME)sd.a: $(__SAN-OBJECTS)
	$(AR) rcs $@ $^

lib$(_LIB-NAME)cd.a: $(__COV-OBJECTS)
	$(AR) rcs $@ $^

$(_TEST-NAME)d: $(_TEST-NAME)d.o lib$(_LIB-NAME)d.a
	$(CC) $(__DEBUG-LDFLAGS) -o $@ $^

$(_TEST-NAME)sd: $(_TEST-NAME)sd.o lib$(_LIB-NAME)sd.a
	$(CC) $(__DEBUG-LDFLAGS) $(__SAN-FLAGS) -o $@ $^

$(_TEST-NAME)cd: $(_TEST-NAME)cd.o lib$(_LIB-NAME)cd.a
	$(CC) $(__DEBUG-LDFLAGS) $(__COV-FLAGS) -o $@ $^

# Test targets
# ------------

san-test.log: $(_TEST-NAME)sd
	@echo "~~~~~~~~~~~~~~~~~ Running sanitizer test ~~~~~~~~~~~~~~~~~~"
	-./$< | tee $@ 2>&1

valgrind-test.log: $(_TEST-NAME)d
	@echo "~~~~~~~~~~~~~~~~~~ Running valgrind test ~~~~~~~~~~~~~~~~~~"
	-valgrind --leak-check=full ./$< | tee $@ 2>&1

coverage.html: $(_TEST-NAME)cd
	@echo "~~~~~~~~~~~~~~~~ Running coverage analysis ~~~~~~~~~~~~~~~~"
	./$<
	gcovr -r . --html --html-details -o $@

# Compound targets
# ----------------

.PHONY: test

default: lib$(_LIB-NAME).a lib$(_LIB-NAME)d.a $(_TEST-NAME)sd $(_TEST-NAME)d

test: valgrind-test.log san-test.log coverage.html

all: \
    lib$(_LIB-NAME).a lib$(_LIB-NAME)d.a lib$(_LIB-NAME)sd.a lib$(_LIB-NAME)cd.a \
    $(_TEST-NAME)d $(_TEST-NAME)sd $(_TEST-NAME)cd \
    coverage.html san-test.log

# Clean targets
# -------------

.PHONY: clean

clean:
	rm -f *.o *.a *.d \
    *.html *.gcda *.gcno *.css \
    $(_TEST-NAME)d $(_TEST-NAME)sd $(_TEST-NAME)cd \
    valgrind-test.log san-test.log coverage.html
